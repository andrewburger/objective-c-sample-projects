//
//  Appliance.m
//  Appliances
//
//  Created by Andrew Burger on 5/2/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "Appliance.h"

@implementation Appliance

@synthesize voltage;

- (id)init {
    
    return [self initWithProductName:@"Unknown"];
    
}

- (id)initWithProductName:(NSString *)pn {
    
    // Call NSObject's init method
    self = [super init];
    
    // Did it return something non-nil?
    if (self) {
        
        // Set the product name
        productName = [pn copy];
        
        // Give voltage a starting value
        [self setVoltage:120];
    }

    return self;

}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"<%@: %d volts>", productName, voltage];
}

- (void)setVoltage:(int)x {
    NSLog(@"setting voltage to %d", x);
    voltage = x;
}


@end
