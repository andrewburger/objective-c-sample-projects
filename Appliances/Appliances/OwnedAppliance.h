//
//  OwnedAppliance.h
//  Appliances
//
//  Created by Andrew Burger on 5/2/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "Appliance.h"

@interface OwnedAppliance : Appliance {
    NSMutableSet *ownerNames;
}
// The designated initializer
- (id)initWithProductName:(NSString *)pn
           firstOwnerName:(NSString *)n;
- (void)addOwnerNamesObject:(NSString *)n;
- (void)removeOwnerNamesObject:(NSString *)n;

@end
