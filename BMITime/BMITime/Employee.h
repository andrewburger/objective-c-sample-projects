//
//  Employee.h
//  BMITime
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
@class Asset;

@interface Employee : Person {
    int employeeID;
    NSMutableSet *assets;
}
@property int employeeID;

- (void)addAssetsObject:(Asset *)a;
- (unsigned int)valueOfAssets;

@end
