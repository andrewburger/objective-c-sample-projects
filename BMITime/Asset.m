//
//  Asset.m
//  BMITime
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "Asset.h"
#import "Employee.h"

@implementation Asset

@synthesize label, resaleValue, holder;

- (NSString *)description {
    if ([self holder]) {
        return [NSString stringWithFormat:@"<%@: $%d, assigned to %@>",
                [self label], [self resaleValue], [self holder]];
    } else {
        return [NSString stringWithFormat:@"<%@: $%d unassigned>",
                [self label], [self resaleValue]];
    }
}

- (void)dealloc {
    NSLog(@"deallocating %@", self);
}

@end
