//
//  Portfolio.m
//  Stocks
//
//  Created by Andrew Burger on 4/18/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "Portfolio.h"
#import "StockHolding.h"

@implementation Portfolio 

- (void) addStockHoldingObject:(StockHolding *)a {
    
    // If Portfolio doesn't exist, create it...
    if (!portfolioStockHoldings) {
        portfolioStockHoldings = [[NSMutableArray alloc] init];
    }
    [portfolioStockHoldings addObject:a];
}
- (float) valueOfPortfolio {
    float sum = 0.0;
    for (StockHolding *s in portfolioStockHoldings) {
        sum += [s valueInDollars];
    }
    return sum;
}

@end
