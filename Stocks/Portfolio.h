//
//  Portfolio.h
//  Stocks
//
//  Created by Andrew Burger on 4/18/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StockHolding;

@interface Portfolio : NSObject {
    NSMutableArray *portfolioStockHoldings;
}

- (void) addStockHoldingObject:(StockHolding *)a;
- (float) valueOfPortfolio;


@end
