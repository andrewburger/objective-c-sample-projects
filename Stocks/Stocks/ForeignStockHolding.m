//
//  ForeignStockHolding.m
//  Stocks
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "ForeignStockHolding.h"

@implementation ForeignStockHolding

@synthesize conversionRate;

- (float)costInDollars {
    float foreignCost = [super costInDollars];
    return foreignCost * [self conversionRate];
}

- (float)valueInDollars {
    float foreignValue = [super valueInDollars];
    return foreignValue * [self conversionRate];
}

@end
