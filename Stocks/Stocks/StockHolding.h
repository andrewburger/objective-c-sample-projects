//
//  StockHolding.h
//  Stocks
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>

// Setup normal instance class and variables
@interface StockHolding : NSObject {
    float purchaseSharePrice;
    float currentSharePrice;
    int numberOfShares;
}

// Standard accessor methods
@property float purchaseSharePrice;
@property float currentSharePrice;
@property int numberOfShares;

// Two custom instance methods
- (float)costInDollars;     // purchaseSharePrice * numberOfShares
- (float)valueInDollars;    // currentSharePrice * numberOfShares

@end
