//
//  StockHolding.m
//  Stocks
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "StockHolding.h"

@implementation StockHolding

@synthesize purchaseSharePrice, currentSharePrice, numberOfShares;

-(float)costInDollars {
    return purchaseSharePrice * numberOfShares;
}

-(float)valueInDollars {
    return currentSharePrice * numberOfShares;
}

@end
