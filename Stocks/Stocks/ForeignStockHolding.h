//
//  ForeignStockHolding.h
//  Stocks
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StockHolding.h"

@interface ForeignStockHolding : StockHolding {
    float conversionRate;
}

@property float conversionRate;

@end
