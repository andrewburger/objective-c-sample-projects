//
//  main.m
//  Stocks
//
//  Created by Andrew Burger on 4/17/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ForeignStockHolding.h"
#import "Portfolio.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // Create three instances of StockHolding
        ForeignStockHolding *stockOne = [[ForeignStockHolding alloc] init];
                                [stockOne setPurchaseSharePrice:2.00];
                                [stockOne setCurrentSharePrice:2.00];
                                [stockOne setNumberOfShares:1];
                                [stockOne setConversionRate:2.0];
        ForeignStockHolding *stockTwo = [[ForeignStockHolding alloc]init];
                                [stockTwo setPurchaseSharePrice:50.00];
                                [stockTwo setCurrentSharePrice:35.00];
                                [stockTwo setNumberOfShares:27];
                                [stockTwo setConversionRate:5.0];
        ForeignStockHolding *stockThree = [[ForeignStockHolding alloc]init];
                                [stockThree setPurchaseSharePrice:33.00];
                                [stockThree setCurrentSharePrice:80.00];
                                [stockThree setNumberOfShares:1000];
                                [stockThree setConversionRate:17.0];
        
        // Setup Stock List
        NSMutableArray *stocks = [[NSMutableArray alloc] init];
        [stocks addObject:stockOne];
        [stocks addObject:stockTwo];
        [stocks addObject:stockThree];
        
        // Put them into the Portfolio
        Portfolio *portfolio = [[Portfolio alloc] init];
        [portfolio addStockHoldingObject:stockOne];
        [portfolio addStockHoldingObject:stockTwo];
        [portfolio addStockHoldingObject:stockThree];
        
        // Print out Stock List
        for (ForeignStockHolding *s in stocks) {
            NSLog(@"Here a stock:\n %f price, %f price, %d stk., \n$%f, $%f with Conversion rate: %f",
                  [s purchaseSharePrice], [s currentSharePrice], [s numberOfShares], [s costInDollars], [s valueInDollars], [s conversionRate]);
        }
        
        // Print out value of entire Stock Porfolio
        NSLog(@"This is the value of the entire portofolio: %f", [portfolio valueOfPortfolio]);
        
        
    }
    return 0;
}

