//
//  NSString+VowelCounting.h
//  VowelCounter
//
//  Created by Andrew Burger on 5/5/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VowelCounting)

- (int)vowelCount;

@end
