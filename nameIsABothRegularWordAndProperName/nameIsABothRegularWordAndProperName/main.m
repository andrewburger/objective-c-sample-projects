//
//  main.m
//  nameIsABothRegularWordAndProperName
//
//  Created by Andrew Burger on 4/16/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // Read in "Proper Name" file as a huge string (ignoring the possibility of an error)
        NSString *nameString
        = [NSString stringWithContentsOfFile:@"/usr/share/dict/propernames"
                                    encoding:NSUTF8StringEncoding
                                       error:NULL];
        
        // Break up the Names into an array of strings
        NSArray *names = [nameString componentsSeparatedByString:@"\n"];
        
        // Read in "Regular Word" file as a huge string (ignoring the possibility of an error)
        NSString *wordString
        = [NSString stringWithContentsOfFile:@"/usr/share/dict/words"
                                    encoding:NSUTF8StringEncoding
                                       error:NULL];
        
        // Break up the Words into an array of strings
        NSArray *words = [wordString componentsSeparatedByString:@"\n"];
            
        // Go through each "Proper Name" in the list
        for (NSString *n in names) {
            
            // See if that name exists in the "Regular Word" list
            for (NSString *w in words) {
                
                // Print out the name and stop the search if there's a match!
                if ([n caseInsensitiveCompare:w] == NSOrderedSame) {
                    NSLog(@"%@", n);
                    break;
                }
            }
        }
    }
    return 0;
}

